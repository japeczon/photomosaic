#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "patchwork.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;

patchwork project;
string beforeImage = "C:\\Users\\Joey\\Documents\\Photomosaic\\1367244409415213056_EvltWtlWYAAVHxJ.jpg";
string sampleDirectory = "C:\\Users\\Joey\\Documents\\Photomosaic\\Sample Images";
Size tileSize(25,25);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);



    project.getTargetImage(beforeImage);

    project.readSampleDirectory(sampleDirectory);

    project.generateSwapTiles(tileSize);

    namedWindow("Result");

    imshow("Result", project.createQuilt(tileSize));
}

MainWindow::~MainWindow()
{
    delete ui;
}
