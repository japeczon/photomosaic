#include "patchwork.h"

patchwork::patchwork()
{
}

void patchwork::getTargetImage(const string imagePath)
{
    if(haveImageReader(imagePath))
        targetImage = imread(imagePath, IMREAD_COLOR);
}

void patchwork::readSampleDirectory(const string path)
{
    directoryFiles.clear();

    for (const auto & entry : filesystem::directory_iterator(path))
        if(haveImageReader(entry.path().string()))
            directoryFiles.push_back(entry.path().string());
}

void patchwork::generateSwapTiles(const Size tile)
{
    Mat image;
    vector<Mat> splitImage;
    string filename;
    pixel addingPixels;

    sampleAverages.clear();

    for (unsigned var1 = 0; var1 < directoryFiles.size(); ++var1)
    {
        addingPixels.red = 0;
        addingPixels.green = 0;
        addingPixels.blue = 0;

        image = imread(directoryFiles.at(var1));
        resize(image,image,tile);
        split(image, splitImage);

        for (int var2 = 0; var2 < tile.height; ++var2)
        {
            for (int var3 = 0; var3 < tile.width; ++var3)
            {
                addingPixels.blue += (int)splitImage[0].at<uchar>(var3,var2);
                addingPixels.green += (int)splitImage[1].at<uchar>(var3,var2);
                addingPixels.red += (int)splitImage[2].at<uchar>(var3,var2);
            }
        }

        splitImage.clear();
        addingPixels.red /= tile.area();
        addingPixels.green /= tile.area();
        addingPixels.blue /= tile.area();
        sampleAverages.push_back(addingPixels);

        filename = "C:/Users/Joey/Documents/Photomosaic/processedImages/";
        filename += to_string(var1);
        filename.append(".jpeg");
        directoryFiles[var1] = filename;
        imwrite(filename,image);
        filename.erase();
    }
}

Mat const patchwork::createQuilt(const Size tile)
{
    vector<Mat> splitImage;
    Mat inProgress;
    Mat result;
    pixel addingPixels;
    int xStart = 0;
    int xIndex = 0;
    int yStart = 0;
    int yIndex = 0;
    float next;
    float closest;
    int closestIndex;

    split(targetImage,splitImage);

    while (yIndex < targetImage.rows)
    {
        while(xStart < targetImage.cols)
        {
            addingPixels.red = 0;
            addingPixels.green = 0;
            addingPixels.blue = 0;

            while(yIndex-yStart < tile.height && yIndex < targetImage.rows)
            {
                while(xIndex-xStart < tile.width && xIndex < targetImage.cols)
                {
                    addingPixels.blue += (int)splitImage[0].at<uchar>(yIndex,xIndex);
                    addingPixels.green += (int)splitImage[1].at<uchar>(yIndex,xIndex);
                    addingPixels.red += (int)splitImage[2].at<uchar>(yIndex,xIndex);
                    ++xIndex;
                }

                xIndex = xStart;
                ++yIndex;
            }

            addingPixels.red /= tile.area();
            addingPixels.green /= tile.area();
            addingPixels.blue /= tile.area();

            closest = FLT_MAX;

            for (unsigned var = 0; var < sampleAverages.size(); ++var)
            {
                next = sqrt(pow(addingPixels.red-sampleAverages[var].red,2)+pow(addingPixels.green-sampleAverages[var].green,2)+pow(addingPixels.blue-sampleAverages[var].blue,2));
                if(next < closest)
                {
                    closest = next;
                    closestIndex = var;
                }
            }

            if(inProgress.empty())
                inProgress = imread(directoryFiles[closestIndex]);
            else
                hconcat(inProgress, imread(directoryFiles[closestIndex]), inProgress);

            xIndex = xStart += tile.width;
            yIndex = yStart;
        }

        if(result.empty())
            result = inProgress;
        else
            vconcat(result, inProgress, result);

        inProgress.release();
        yIndex = yStart += tile.height;
        xIndex = xStart = 0;
    }
    cout << "done";
    return result;
}
