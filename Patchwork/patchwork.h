#ifndef PATCHWORK_H
#define PATCHWORK_H


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <filesystem>
#include <vector>
#include <cmath>
#include <iostream>

using namespace std;
using namespace cv;

struct pixel
{
    int blue=0;
    int green=0;
    int red=0;
};

class patchwork
{
public:
    Mat targetImage;
    vector<string> directoryFiles;
    vector<pixel> sampleAverages;
    vector<pixel> objective;

    patchwork();
    void getTargetImage(const string);
    void readSampleDirectory(const string);
    void generateSwapTiles(const Size);
    Mat const createQuilt(const Size);

private:
};

void getTargetImage(const string);
void readSampleDirectory(const string);
void generateSwapTiles(const Size);
Mat const createQuilt(const Size);

#endif // PATCHWORK_H
